import React from "react"
import { Container } from "reactstrap"

//Import Breadcrumb
import Breadcrumbs from "../components/breadcrumb"

const Apps = () => {
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          <Breadcrumbs title="Apps" breadcrumbItem="Under Construction" />
        </Container>
      </div>
    </React.Fragment>
  )
}

export default Apps
